# AVR Scheduler

A simple, preemptive task-scheduler for AVR (ATmega) MCUs (tested on ATmega328).

Copyright &copy; 2023 Jan Bellenberg