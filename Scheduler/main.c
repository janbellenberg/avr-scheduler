#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "incl/scheduler.h"
#include "incl/mutex.h"
#include "incl/serial.h"

void setup();

int main(void)
{
  os_boot(setup);
}

void ledTask(void *param) {
	while(1) {
		PORTB ^= (1 << PB5);	// toggle pin 13
		sleep(500);
	}
}

void ledTask2(void *param) {
	while(1) {
		PORTC ^= (1 << PC0);	// toggle a0
		sleep(100);
	}
}

uint8_t LED_TASK2_PID = 0;

void setup()
{
  mutex_add_config(MUTEX_UART, 1);

  // init ports
  DDRB = (1 << PB4) | (1 << PB5);	// PB4 (pin12) & PB5 (pin13) as output
  DDRC = 1 << PC0;
  DDRD = 0;

  PORTB = 1 << PB5;
  PORTC = 1 << PC0;
  PORTD = 1 << PD2;		// activate pull up resistor for PD2
  
  // configure INT0
  EICRA = (1 << ISC01);
  EIMSK |= (1 << INT0);
  
  // Aktiviere serielle Schnittstelle
  USART_Init(9600);
  
  // start tasks
  SERIAL_PID = startTask("UART", &USART_Thread, 256, NULL);
  setPriority(SERIAL_PID, HIGH);
  
  startTask("LED1", &ledTask, 192, NULL);
  LED_TASK2_PID = startTask("LED2", &ledTask2, 192, NULL);
}

volatile uint8_t ledTask2Running = 1;
volatile uint64_t lastTicks = 0;

ISR(INT0_vect) {	// key press ISR
	if(lastTicks - os_get_real_ticks() >= 10) {		// only accept key presses after a certain amount of time
		lastTicks = os_get_real_ticks();
		
		// toggle task state
		if(ledTask2Running) {
			PAUSE(LED_TASK2_PID);
			ledTask2Running = 0;
		} else {
			RESUME(LED_TASK2_PID);
			ledTask2Running = 1;
		}
	}
}
