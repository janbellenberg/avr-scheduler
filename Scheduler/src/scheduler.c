/**
 * This file contains some general methods and variables for the scheduling
 * @see scheduler.h and scheduler_internal.h
 * @see https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf
 */

#include "incl/scheduler.h"
#include "incl/scheduler_internal.h"
#include "incl/serial.h"

#include <util/delay.h>

volatile KernelStatus kernel;
volatile void *current_stack = NULL;
char MEMORY[MEMORY_SIZE];
uint64_t _os_time = 0;
uint64_t _idle_time = 0;
uint16_t _timerDiff = 0;

// ISR before WDT reset
ISR(WDT_vect) {
#ifdef TASKMGR
	LOG("WDTI");
#endif
}

#ifdef TASKMGR
void *calcDemoTask(void *param) {
	uint32_t val = 0;
	while(val < 4000000) {
		val++;
		_delay_us(1);
	}
	LOG("DONE");
	return NULL;
}

void _taskmgrTask(void *param) {
	while(1) {
		if(USART_Data_Available()) {
			char recv = USART_Read();
			if(recv == 'S') {
				os_shutdown(NULL);
				return;
			} else if(recv == 'R') {
				os_reboot();
				return;
			} else if(recv == 'C') {
				startMicrotask("CALC", &calcDemoTask, 128, NULL, NULL);
			}
		}
		
		sendPackage(ID_OS_TIME, 0, kernel.os_time);
		sendPackage(ID_OS_USAGE, 0, cpuUsage());
		
		Task *task = kernel.firstTask;
		do {
			sendPackage(ID_TASK_TIME, task->PID, task->usedSlots);
		} while((task = task->next) != kernel.firstTask);
		
		sleep(500);
	}
}
#endif

void idle(void *param)
{
  // enable power down mode
  // datasheet p.37
  SMCR = (1 << SE) | (1 << SM1) | (1 << SM0);
  
  asm volatile(
      "wdr \n"
      "sei \n"
      "sleep \n");
	 
  while(1);
}