#include "incl/scheduler.h"
#include "incl/scheduler_internal.h"

void sendSignal(uint8_t PID, uint8_t signal, void *value)
{
	CHECK_KERNEL_BOOT

	if (kernel.currentTask->PID == PID) return;

	Task *task = _getTaskByID(PID);

	if (task == NULL) return;

	if (signal == SIG_INTERRUPT) {
		task->REQUEST_INTERRUPTION = 1;
	}

	Signal *sig = (Signal *)malloc(sizeof(Signal));
	sig->signal = signal;
	sig->payload = value;
	sig->senderPID = kernel.currentTask->PID;
	sig->next = task->lastSignal;
	task->lastSignal = sig;

	if (task->signalHandler != NULL && ((uint16_t)task->sp) - ((uint16_t)task->stack) > 1)
	{
		memcpy(task->sp - 2, task->sp, 33);
		task->sp = task->sp - 2;
		task->sp[35] = (uint16_t)task->signalHandler & 0xFF;
		task->sp[34] = (uint16_t)task->signalHandler >> 8;
	}
}

Signal *getNextSignal()
{
	CHECK_KERNEL_BOOT_WERROR(NULL)
	Signal *next = kernel.currentTask->lastSignal;
	if (next != NULL) {
		kernel.currentTask->lastSignal = next->next;
	}
	return next;
}

void registerSignalHandler(FunctionHandle handle)
{
	CHECK_KERNEL_BOOT
	kernel.currentTask->signalHandler = handle;
}

void requestInterruption(uint8_t PID)
{
	sendSignal(PID, SIG_INTERRUPT, NULL);
}

uint8_t isInterruptionRequested()
{
	CHECK_KERNEL_BOOT_WERROR(0);
	return kernel.currentTask->REQUEST_INTERRUPTION;
}