#include "incl/scheduler.h"
#include "incl/scheduler_internal.h"

#ifdef SCHEDULER_FIFO
void schedule(void) {

	// Implementation of First-In-First-Out

	// search all task in the order, they were created
	// and always schedule the first task that is not inactive.
	// IDLE will only be run, if there are no other tasks or
	// all other tasks are suspended.

	kernel.waitForDispatch = 0;
	
	if (kernel.existingTasks < 1) return;
	
	kernel.os_time += OCR2A - _timerDiff;

	Task *next_task = kernel.firstTask->next; // Start with first non idle task (firstTask->next is only the idle task if only the idle task exists)

	while(next_task != kernel.firstTask) {
		if (next_task->status == SUSPENDED && next_task->resumeAt < kernel.os_time && next_task->resumeAt != 0) {
			next_task->status = READY;
		}

		if(next_task->status == READY) break;

		next_task = next_task->next;
	}

	// If while cycles back to idle task without any other task existing / being ready then schedule idle task
	if(next_task == kernel.firstTask) {
		_resetIdleTaskStack();
	}

	kernel.currentTask = next_task;
	kernel.currentTask->usedSlots += OCR2A - _timerDiff;
	_timerDiff = 0;
	current_stack = &kernel.currentTask->sp;
}
#endif