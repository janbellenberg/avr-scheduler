#include "incl/scheduler.h"
#include "incl/scheduler_internal.h"

#if defined SCHEDULER_RR_PRIO || defined SCHEDULER_SJF
void schedule(void) {
	//Implementation of priority based Round-Robin and Shortest-Job-First

	// search all task in the order, they were created and
	// preferably chooses the one that satisfies the combination of having a high
	// priority and/or waited longer than other tasks.
	// Shortest-Job-First additionally considers the type of task into the scoring.
	// Micro tasks will then have a greater score.

	// IDLE will only be run, if there are no other tasks or
	// all other tasks are suspended.

	kernel.waitForDispatch = 0;
	
	if (kernel.existingTasks < 1) return;

	kernel.os_time += OCR2A - _timerDiff;

	// schedule next task
	Task *curr_pointer = kernel.firstTask->next; // Start with first non idle task (firstTask->next is only the idle task if only the idle task exists)

	Task* potential_next_task = NULL;
	double priority_score = 0.0;
	double tmp = 0.0;

	while(curr_pointer != kernel.firstTask) {
		// resume tasks after sleep
		if (curr_pointer->status == SUSPENDED && curr_pointer->resumeAt < kernel.os_time && curr_pointer->resumeAt != 0) {
			curr_pointer->status = READY;
		}

		if(curr_pointer->status == READY) {
			//Calculate priority score based on the priority and the amount of scheduling cycles the task is already waiting.
			tmp = (double)(curr_pointer->priority) * (double)(0.5*curr_pointer->waitingCycles + 1);

			#ifdef SCHEDULER_SJF // Additional weight to micro tasks in shortest job first
			tmp *= curr_pointer->type == MICROTASK ? 2.0 : 0.5;
			#endif

			// Remember the task with the best scoring
			if(tmp > priority_score) {
				potential_next_task = curr_pointer;
				priority_score = tmp;
			}
		}

		curr_pointer = curr_pointer->next;
	}

	if(potential_next_task != NULL) {
		//If next task was found then increment the counter property (waitingCycles) of all other ready tasks.
		curr_pointer = kernel.firstTask->next;

		while (curr_pointer != kernel.firstTask) {
			if (curr_pointer != potential_next_task) {
				curr_pointer->waitingCycles++;
			}

			curr_pointer = curr_pointer->next;
		}

		kernel.currentTask = potential_next_task;
		} else {
		// If no available task was found then reinsert idle task
		kernel.currentTask = kernel.firstTask;
		_resetIdleTaskStack();
	}
	
	kernel.currentTask->usedSlots += OCR2A - _timerDiff;
	_timerDiff = 0;
	kernel.currentTask->waitingCycles = 0;
	current_stack = &kernel.currentTask->sp;
}
#endif
