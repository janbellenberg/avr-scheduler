#include "incl/scheduler.h"
#include "incl/scheduler_internal.h"

#ifdef SCHEDULER_RR
void schedule(void)
{
	// Implementation of Round-Robin

	// go through all task in the order, they were created
	// IDLE will only be run, if there are no other tasks or
	// all other tasks are suspended.

	kernel.waitForDispatch = 0;
	
	if (kernel.existingTasks < 1) return;
	kernel.os_time += OCR2A - _timerDiff;

	// schedule idle task, if no other task is ready
	if (kernel.existingTasks == 1)
	{
		kernel.currentTask = kernel.firstTask;
		kernel.currentTask->usedSlots += OCR2A - _timerDiff;
		_timerDiff = 0;
		_resetIdleTaskStack();
		current_stack = &(kernel.currentTask->sp);
		return;
	}

	// schedule next task
	Task *next_task = kernel.currentTask;

	do
	{
		next_task = next_task->next;

		if (next_task->status == SUSPENDED && next_task->resumeAt < kernel.os_time && next_task->resumeAt != 0)
		{
			next_task->status = READY;

			#ifdef TASKMGR
			sendPackage(ID_RESUME_TASK, next_task->PID, 0);
			#endif
		}

		// if scheduler looped over all tasks and didn't find another runnable task
		// and the current task can't be scheduled again.
		if (next_task == kernel.currentTask && (next_task->status != READY || next_task->PID == 0))
		{
			next_task = kernel.firstTask;
			_resetIdleTaskStack();
			break;
		}

	} while (next_task->status != READY || next_task->PID == 0);

	kernel.currentTask = next_task;
	kernel.currentTask->usedSlots += OCR2A - _timerDiff;
	_timerDiff = 0;
	current_stack = &kernel.currentTask->sp;
}
#endif