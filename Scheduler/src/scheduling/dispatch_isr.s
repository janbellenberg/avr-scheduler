
/*
 * dispatch_isr.s contains the interrupt service routine
 * for the Timer2, in which the context switch happens.
 */ 

#include <avr/io.h>

.global TIMER2_COMPA_vect
TIMER2_COMPA_vect:
	wdr			; reset Watchdog timer

	; save context to stack
	push r31
	push r30
	push r29
	push r28
	push r27
	push r26
	push r25
	push r24
	push r23
	push r22
	push r21
	push r20
	push r19
	push r18
	push r17
	push r16
	push r15
	push r14
	push r13
	push r12
	push r11
	push r10
	push r9 
	push r8 
	push r7 
	push r6 
	push r5 
	push r4 
	push r3 
	push r2 
	push r1 
	push r0

	; check if there was already a task
	; or if we are just booting up
	;  -> in this case we can skip to loading a new task
	ldi r16, 0
	lds r17, current_stack
	lds r18, current_stack+1
	or r17, r18					; merge lower & upper byte
	cp r17, r16					; compare result of or with 0
	breq loadTask				; jump of result is 0
	; otherwise: continue

	; Push SREG
	in   r0, 0x3f
	push r0

	; Load address of next task
	lds  r26, current_stack
	lds  r27, current_stack+1

	; store stack pointer (SPL & SPH) 
	; because r26 & r27 (-> X-Register) point to the current task,
	; SPL & SPH are written to Task::sp (stack pointer),
	; because this attr is the first
	in   r0, 0x3d ; SPL
	st   X+, r0
	in   r0, 0x3e ; SPH
	st   X+, r0

	loadTask:
	; determine new task
	call schedule

	; load new address of next task
	lds  r26, current_stack
	lds  r27, current_stack+1

	; load stack pointer of new task (just as before)
	ld   r0, X+
	out  0x3d, r0
	ld r0, X+
	out 0x3e, r0

	; restore SREG
	pop r31
	out 0x3f, r31 ; 0x3f -> SREG

	; restore context
	pop  r0
	pop  r1
	pop  r2
	pop  r3
	pop  r4
	pop  r5
	pop  r6
	pop  r7
	pop  r8
	pop  r9
	pop  r10 
	pop  r11 
	pop  r12 
	pop  r13 
	pop  r14 
	pop  r15 
	pop  r16 
	pop  r17 
	pop  r18 
	pop  r19 
	pop  r20 
	pop  r21 
	pop  r22 
	pop  r23 
	pop  r24 
	pop  r25 
	pop  r26 
	pop  r27 
	pop  r28 
	pop  r29 
	pop  r30 
	pop  r31 
	wdr
	reti		; return from interrupt into next task
.end
