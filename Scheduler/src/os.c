#include "incl/scheduler.h"
#include "incl/scheduler_internal.h"

#include <avr/wdt.h>

void os_boot(FunctionHandle setup)
{
	if (kernel.bootupComplete) return;

	// disable all interrupts
	asm volatile("cli");

	// check if the mcu starts because of brown-out detection
	if (MCUSR & (1 << BORF)) { 
		MCUSR &= ~ (1 << BORF);
		LOG("BODT");
	}

	// initialize memory
	__malloc_heap_start = MEMORY;
	__malloc_heap_end = MEMORY + MEMORY_SIZE - 1;
	memset(MEMORY, 0, MEMORY_SIZE);

	// initialize kernel
	kernel.bootupComplete = 0x01;
	kernel.existingTasks = 0x00;
	kernel.os_time = 0x00;
	kernel.firstTask = NULL;
	kernel.lastTask = NULL;
	kernel.waitForDispatch = 0x00;

	// register idle task
	startTask("IDLE", &idle, IDLE_TASK_STACK, NULL);
	kernel.currentTask = kernel.firstTask;
	kernel.firstTask->type = IDLE;
	
	// register taskmanager task
#ifdef TASKMGR
	startTask("TMGR", _taskmgrTask, 192, NULL);
	sendPackage(ID_FCPU, 0, F_CPU);
#endif

	// setup IDLE resume timer (timer2) (datasheet p.127-131)
	TCCR2A = (1 << WGM21); // clear timer on compare match
	TCCR2B = (1 << CS22) | (1 << CS21) | (1 << CS20);		// prescaler 1024
	TIMSK2 = (1 << OCIE2A);		// enable interrupt
	OCR2A = 255;					// interrupt after 255 events -> 30.6Hz, every 32ms

	// initialize watchdog (datasheet p.47)
	#ifdef WATCHDOG
	MCUSR &= ~(1 << WDRF);
	asm volatile(
		"wdr \n\t"
		"sts %[wdtcsr], %1 \n\t"
		"sts %[wdtcsr], %2 \n\t" ::
		[wdtcsr] "n"(_SFR_MEM_ADDR(WDTCSR)),
		"r"((uint8_t)((1 << WDCE) | (1 << WDE))),
		"r"((uint8_t)((1 << WDIE) | (1 << WDE) | (1 << WDP2) | (1 << WDP1))));
	#endif

	// run the setup function
	if (setup != NULL) {
		setup();
	}

#ifdef TASKMGR
	LOG("BOOT");
#endif

	// start scheduling
	YIELD();
}

void os_shutdown(FunctionHandle afterShutdown)
{
	CHECK_KERNEL_BOOT

	if (afterShutdown == NULL) {
		asm volatile("cli");
		wdt_disable();
		
		TCCR2A = 0;
		TCCR2B = 0;
		TIMSK2 = 0;
		OCR2A  = 0;
		
		DDRB = DDRC = DDRD = 0;
		PORTB = PORTC = PORTD = 0;

		// enable power down mode
		// see datasheet p.37
		SMCR = (1 << SM1) | (1 << SE);
		asm volatile("sleep");
		while(1);
		
	} else {
		// stop all tasks
		Task *task = kernel.firstTask;
		while (kernel.existingTasks > 0)
		{
			Task *next = task->next;
			stopTask(task->PID);
			task = next;
		}

		// register a task, which stops the timer and runs the afterShutdown method.
		startTask("", &_handleTimerShutdown, AFTER_SHUTDOWN_STACK_SIZE, afterShutdown);

		kernel.bootupComplete = 0x00;
		kernel.existingTasks = 0x01;
		kernel.waitForDispatch = 0x00;

		YIELD();
	}
}

void os_reboot(void)
{
	// disable schedule timer to trigger the watchdog.
	TCCR2A = 0;
	TCCR2B = 0;
	TIMSK2 = 0;
	OCR2A	 = 0;
	asm volatile("sei");
	while (1); // this will trigger watchdog
}

void _handleTimerShutdown(void *functionPointer)
{
	// stop timer
	TCCR2A = 0;
	TCCR2B = 0;
	TIMSK2 = 0;
	OCR2A  = 0;

	free(kernel.firstTask->stack);

	kernel.os_time = 0x00;
	kernel.firstTask = NULL;
	kernel.lastTask = NULL;

	((FunctionHandle)functionPointer)();
}

uint8_t cpuUsage(void)
{
	if (!kernel.bootupComplete)
	return -1;

	// return 100%, if no time elapsed, since last check.
	if (kernel.os_time - _os_time < 1) {
		return 100;
	}

	double idle = ((double)(kernel.firstTask->usedSlots - _idle_time)) / (kernel.os_time - _os_time);
	_idle_time = kernel.firstTask->usedSlots;
	_os_time = kernel.os_time;

	return (1.0 - idle) * 100;
}

double os_get_up_time()
{
	CHECK_KERNEL_BOOT_WERROR(-1)
	// timer prescaler = 1024
	return kernel.os_time * 1024.0 / F_CPU;
}

uint64_t os_get_real_ticks() {
	return kernel.os_time >> 8;
}