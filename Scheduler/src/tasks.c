#include "incl/scheduler.h"
#include "incl/scheduler_internal.h"

uint8_t startTask(char name[TASK_NAME_LENGTH+1], TaskHandle function, uint16_t stackSize, volatile void *param)
{
    Task *task = _configureTask(name, function, stackSize, param);
    if (task == NULL)
        return 0xff;
    task->type = TASK;
    return task->PID;
}

uint8_t startMicrotask(char name[TASK_NAME_LENGTH+1], MicrotaskHandle function, uint16_t stackSize, volatile void *param, void **result)
{
    Task *task = _configureTask(name, function, stackSize, param);
    if (task == NULL)
        return 0xff;
    task->result = result;
    task->type = MICROTASK;
    return task->PID;
}

void await(uint8_t PID)
{
    BEGIN_KERNEL_OPERATION

    Task *task = _getTaskByID(PID);
    // await is only possible, if task current_task is parent of task
    if (task == NULL || task->parent == NULL || task->parent->PID != kernel.currentTask->PID)
    {
		END_KERNEL_OPERATION
        return;
    }

    // set flags
    task->RESUME_PARENT_ON_EXIT = 1;
    kernel.currentTask->resumeAt = -1;
    kernel.currentTask->status = SUSPENDED;

#ifdef TASKMGR
    sendPackage(ID_SUSPEND_TASK, PID, 0);
#endif

    END_KERNEL_OPERATION
    YIELD();
}

Task *_configureTask(char name[TASK_NAME_LENGTH+1], void *function, uint16_t stackSize, volatile void *param)
{
    BEGIN_KERNEL_OPERATION_WERROR(NULL)

    // create new task item
    Task *task = malloc(sizeof(Task));
    if (task == NULL)
    {
        END_KERNEL_OPERATION
        return NULL;
    }
    kernel.existingTasks++;

    task->PID = kernel.pid_counter++;
    strcpy(task->name, name);
    task->stackSize = stackSize;
    task->status = READY;
    task->parent = kernel.currentTask;
    task->resumeAt = 0;
    task->startedAt = kernel.os_time;
    task->usedSlots = 0;
    task->param = param;
    task->result = NULL;
    task->priority = DEFAULT;


    task->RESUME_PARENT_ON_EXIT = 0;

    // allocate stack
    task->stack = malloc(task->stackSize);
    if (task->stack == NULL)
    {
        // rollback, if not enough memory
        free(task);
        kernel.existingTasks--;
        END_KERNEL_OPERATION;
        return NULL;
    }

    /**
     * configure stack
     * lowest address> <unused space>
     * HEAD> sreg
     * r31 - r26
     * r24 & r25 -> params
     * r23 - r0
     * 16bit function pointer (return address for ISR)
     * highest address> 16bit _handleExit pointer (return address for task)
     */
    task->sp = task->stack + (stackSize - 1);

    // make space for return address, pc, sreg, and 32 registers
    task->sp[0] = (uint16_t)_handleExit & 0xFF;
    task->sp[-1] = (uint16_t)_handleExit >> 8;

    task->sp[-2] = (uint16_t)function & 0xFF;
    task->sp[-3] = (uint16_t)function >> 8;
    for (int i = -4; i > -36; i--)
    {
        task->sp[i] = 0;
    }

    // add params
    task->sp[-11] = (uint16_t)param & 0xff;                // r24
    task->sp[-10] = ((uint16_t)param >> 8) & 0xff; // r25

    task->sp[-36] = 0x80; // sreg, interrupts enabled

    task->sp = task->sp - 37;

    // add to linked list
    if (kernel.firstTask == NULL || kernel.lastTask == NULL || kernel.existingTasks == 0)
    {
        // if list is empty
        kernel.firstTask = task;
        kernel.lastTask = task;
        task->next = task;
        task->previous = task;
    }
    else
    {
        task->previous = kernel.lastTask;
        task->next = kernel.firstTask;
        task->previous->next = task;
        kernel.lastTask = task;
    }

#ifdef TASKMGR
    sendPackage(ID_START_TASK, 0, task->PID);
    sendString(ID_TASK_NAME, task->PID, task->name);
    sendPackage(ID_RESUME_TASK, task->PID, 0);
#endif

    END_KERNEL_OPERATION
    return task;
}

void _configureFork(void)
{
	Task *newTask = _configureTask("        ", NULL, kernel.currentTask->stackSize, kernel.currentTask->param);
	newTask->sp = newTask->sp - 2;
	
	// copy registers -> SPH & SPL are in kernel.currentTask->sp
	memcpy(newTask->sp + 1, kernel.currentTask->sp + 1, 33); // copy r0-r31 + sreg
	memcpy(newTask->sp + 36, kernel.currentTask->sp + 34, 2);	// copy return value
	
	// set entrypoint for new task
	newTask->sp[34] = ((uint16_t) &_forkEnd) >> 8;
	newTask->sp[35] = ((uint16_t) &_forkEnd) & 0xFF;
	
	// copy data from old task
	strcpy(newTask->name, kernel.currentTask->name);
	newTask->parent = kernel.currentTask->parent;
	newTask->priority = kernel.currentTask->priority;
	newTask->result = kernel.currentTask->result;
	newTask->signalHandler = kernel.currentTask->signalHandler;
	newTask->type = kernel.currentTask->type;
	
	newTask->REQUEST_INTERRUPTION = kernel.currentTask->REQUEST_INTERRUPTION;
	newTask->RESUME_PARENT_ON_EXIT = kernel.currentTask->RESUME_PARENT_ON_EXIT;
}

void _resetIdleTaskStack(void)
{
    kernel.firstTask->sp = kernel.firstTask->stack + (IDLE_TASK_STACK - 1);

    // make space for return address, pc, sreg, and 32 registers
    kernel.firstTask->sp[0] = (uint16_t)_handleExit & 0xFF;
    kernel.firstTask->sp[-1] = (uint16_t)_handleExit >> 8;

    kernel.firstTask->sp[-2] = (uint16_t)&idle & 0xFF;
    kernel.firstTask->sp[-3] = (uint16_t)&idle >> 8;
    for (int i = -4; i > -36; i--)
    {
        kernel.firstTask->sp[i] = 0;
    }

    kernel.firstTask->sp[-36] = 0x80; // sreg, interrupts enabled

    kernel.firstTask->sp = kernel.firstTask->sp - 37;
}

void stopTask(uint8_t PID)
{
    BEGIN_KERNEL_OPERATION
    Task *task = _getTaskByID(PID);
    if (task == NULL)
        return;

    // stop sub tasks
    Task *potentialSubTask = kernel.firstTask;
    while (potentialSubTask != NULL)
    {
        Task *next = potentialSubTask->next;
        if (next == task)
            break;

        if (potentialSubTask->parent->PID == kernel.currentTask->PID)
        {
            stopTask(potentialSubTask->PID);
        }
        potentialSubTask = next;
    }

    kernel.existingTasks--;

    // manage linked list of tasks
    if (kernel.existingTasks == 0)
    {
        // list is now empty
        kernel.firstTask = NULL;
        kernel.lastTask = NULL;
        kernel.currentTask = NULL;
    } else {
        // if the task was the first task
        if (task == kernel.firstTask) {
            kernel.firstTask = task->next;
        }

        // if the task was the last task
        if (task == kernel.lastTask)  {
            kernel.lastTask = task->previous;
        }

        // if the task was the current task
        if (task == kernel.currentTask) {
            kernel.currentTask = NULL;
        }

        // connect previous and next
        task->previous->next = task->next;
        task->next->previous = task->previous;
    }

    // free memory
    free(task->stack);
    free(task);

#ifdef TASKMGR
    sendPackage(ID_STOP_TASK, PID, 0);
#endif

    END_KERNEL_OPERATION
}

void _handleExit(void)
{
	// read return value from r24 & r25
	void *result;
	asm volatile(
		"cli\n"
		"movw %0, r24"
		: "=r"(result));

	BEGIN_KERNEL_OPERATION

	// send result, if microtask
	if (kernel.currentTask->result != NULL) {
		*(kernel.currentTask->result) = result;
	}

	//    resume parent task if awaiting
	if (kernel.currentTask->parent != NULL && kernel.currentTask->RESUME_PARENT_ON_EXIT)
	{
		kernel.currentTask->parent->resumeAt = 0;
		kernel.currentTask->parent->status = READY;
	}

	// clean current task up
	stopTask(kernel.currentTask->PID);
	current_stack = NULL;

	END_KERNEL_OPERATION
	YIELD();
}

Task *_getTaskByID(uint8_t PID)
{
    CHECK_KERNEL_BOOT_WERROR(NULL);

    Task *task = kernel.firstTask;

    // loop through all tasks
    // until the correct PID appears
    while (task->PID != PID) {
        task = task->next;
        if (task->PID == 0) return 0;
    }

    return task;
}

void YIELD(void)
{
    CHECK_KERNEL_BOOT
    asm("sei");     // enable global interrupt
    _timerDiff = OCR2A - TCNT2;
    TCNT2 = 0xff; // set timer just before overflow (datasheet p.87)
    kernel.waitForDispatch = 1;
    while (kernel.waitForDispatch) {  // wait for overflow
        asm volatile("wdr"); // reset watchdog
    }
}

void SUSPEND(void)
{
    CHECK_KERNEL_BOOT
    
    kernel.currentTask->resumeAt = 0;
    kernel.currentTask->status = SUSPENDED;
    YIELD();

#ifdef TASKMGR
    sendPackage(ID_SUSPEND_TASK, kernel.currentTask->PID, 0);
#endif
}

void PAUSE(uint8_t PID)
{
    CHECK_KERNEL_BOOT
    Task *task = _getTaskByID(PID);
    
    if(task == NULL) return;

    task->resumeAt = 0;
    task->status = SUSPENDED;

#ifdef TASKMGR
    sendPackage(ID_SUSPEND_TASK, PID, 0);
#endif
}

void RESUME(uint8_t PID)
{
    CHECK_KERNEL_BOOT
    Task *task = _getTaskByID(PID);
    if(task == NULL) return;
    task->resumeAt = 0;
    task->status = READY;

#ifdef TASKMGR
    sendPackage(ID_RESUME_TASK, PID, 0);
#endif
}

void sleep(uint16_t milliseconds)
{
    CHECK_KERNEL_BOOT
    // ms -> seconds(/1000) -> cpu ticks(*8MHz) -> timer ticks(/1024)
    // ! do not change the order of the formula, because of the range of double
    uint64_t ticks = ceil(milliseconds / 1000.0 * F_CPU / 1024.0);
    kernel.currentTask->resumeAt = kernel.os_time + ticks;
    kernel.currentTask->status = SUSPENDED;

#ifdef TASKMGR
    sendPackage(ID_SUSPEND_TASK, kernel.currentTask->PID, 0);
#endif

    YIELD();
}

void setPriority(uint8_t PID, Priority priority) {
	Task* task = _getTaskByID(PID);
	if(task != NULL) {
		task->priority = priority;
	}
}
