#include "incl/taskmanager.h"
#include "incl/serial.h"

void sendPackage(uint8_t ID, uint8_t Ref, uint64_t Payload) {
	// do not log state changes for usart task
	if((ID == ID_SUSPEND_TASK || ID == ID_RESUME_TASK) && Ref == SERIAL_PID) {
		return;
	}
	
	Converter converter;
	converter.data.ID = ID;
	converter.data.Ref = Ref;
	converter.data.Payload = Payload;
	
	asm volatile("cli");
	USART_Send(converter.raw.byte0);
	USART_Send(converter.raw.byte1);
	USART_Send(converter.raw.byte2);
	USART_Send(converter.raw.byte3);
	USART_Send(converter.raw.byte4);
	USART_Send(converter.raw.byte5);
	asm volatile("sei");
}

void sendString(uint8_t ID, uint8_t Ref, char Payload[4]) {
	asm volatile("cli");
	USART_Send(ID);
	USART_Send(Ref);
	USART_Send(Payload[0]);
	USART_Send(Payload[1]);
	USART_Send(Payload[2]);
	USART_Send(Payload[3]);
	asm volatile("sei");
}