/*
 * fork.s contains the the forkTask-Method, which duplicates
 * the current task. 
 */ 
.global forkTask
.global _forkEnd

forkTask:
	cli
	push r31	; backup registers
	push r30
	push r29
	push r28
	push r27
	push r26
	push r25
	push r24
	push r23
	push r22
	push r21
	push r20
	push r19
	push r18
	push r17
	push r16
	push r15
	push r14
	push r13
	push r12
	push r11
	push r10
	push r9 
	push r8 
	push r7 
	push r6 
	push r5 
	push r4 
	push r3 
	push r2 
	push r1 
	push r0

	in   r0, 0x3f	; push SREG
	push r0

	; backup sph & spl
	; Load address of task struct
	lds  r26, current_stack
	lds  r27, current_stack+1

	; store stack pointer (SPL & SPH) 
	; because r26 & r27 (-> X-Register) point to the current task,
	; SPL & SPH are written to Task::sp (stack pointer),
	; because this attr is the first
	in   r0, 0x3d ; SPL
	st   X+, r0
	in   r0, 0x3e ; SPH
	st   X+, r0

	; duplicate task structure
	call _configureFork

	; restore SREG
	pop r31
	out 0x3f, r31 ; 0x3f -> SREG

	; restore register
	pop  r0
	pop  r1
	pop  r2
	pop  r3
	pop  r4
	pop  r5
	pop  r6
	pop  r7
	pop  r8
	pop  r9
	pop  r10 
	pop  r11 
	pop  r12 
	pop  r13 
	pop  r14 
	pop  r15 
	pop  r16 
	pop  r17 
	pop  r18 
	pop  r19 
	pop  r20 
	pop  r21 
	pop  r22 
	pop  r23 
	pop  r24 
	pop  r25 
	pop  r26 
	pop  r27 
	pop  r28 
	pop  r29 
	pop  r30 
	pop  r31 

	wdr

	ldi r24, 0x00	; returns 0 if current task
	sei
	ret

	_forkEnd:
	ldi r24, 0x01	; returns 1 if child task
	sei
	ret