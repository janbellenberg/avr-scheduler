#include "incl/mutex.h"

static MutexItem *first = NULL; ///< entrypoint for the linked list

static MutexItem *_find(uint8_t ID);   ///< find an item by the ID
static void _add(MutexItem *item);     ///< add a new item to the linked list
static MutexItem *_last();                     ///< get the last item of the linked list

void mutex_add_config(uint8_t ID, uint8_t maxConcurrentClients)
{
    MutexItem *item = _find(ID); // check if config block already exists

    if (item == NULL)
    {
        item = (MutexItem *)malloc(sizeof(MutexItem));
        if (item == NULL) return; // malloc error
        _add(item);
    }

    item->ID = ID;
    item->currentClients = 0x00;
    item->isInWriteMode = 0x00;
    item->maxClients = maxConcurrentClients;
    item->next = NULL;
}

uint8_t mutex_available(uint8_t ID, uint8_t clients)
{
    asm volatile("cli"); // disable interrupts
    uint8_t result = 0x00;
    MutexItem *item = _find(ID);

    if (item != NULL && (item->maxClients <= item->currentClients + clients || item->maxClients == 0))
    {
        if (item->currentClients < 1 && clients == 1)
            result = M_WRITE;
        else
            result = M_READ;
    }

    asm volatile("sei"); // enable interrupts
    return result;
}

uint8_t mutex_acquire(uint8_t ID, uint8_t clients, uint8_t mode)
{
    MutexItem *item = _find(ID);
    if (item == NULL) return 0;
    if (mutex_available(ID, clients) < mode) return 0; // check if required mode is available

    asm volatile("cli"); // disable interrupts

    // update config
    item->currentClients += clients;
    if (clients == 1 && mode == M_WRITE)
    {
        item->isInWriteMode = 1;
    }
    asm volatile("sei"); // enable interrupts

    return 1;
}

void mutex_release(uint8_t ID, uint8_t clients)
{
    MutexItem *item = _find(ID);
    if (item == NULL) return;

    asm volatile("cli"); // disable interrupts
    if (clients >= item->currentClients)
        item->currentClients = 0;
    else
        item->currentClients -= clients;

    if (item->isInWriteMode)
        item->isInWriteMode = 0;

    asm volatile("sei"); // enable interrupts
}

static MutexItem *_find(uint8_t ID)
{
    MutexItem *result = first;

    while (result != NULL && result->ID != ID) {
        result = result->next;
    }

    return result;
}

static void _add(MutexItem *item)
{
    if (first == NULL) {
        item->previous = NULL;
        first = item;
    } else {
        MutexItem *last = _last();
        item->previous = last;
        last->next = item;
    }
}

static MutexItem *_last()
{
    if (first == NULL)
        return NULL;

    MutexItem *result = first;

    while (result->next != NULL) {
        result = result->next;
    }

    return result;
}