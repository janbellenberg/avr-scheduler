#include <avr/io.h>
#include <string.h>

#include "incl/scheduler.h"
#include "incl/serial.h"
#include "incl/settings.h"
#include "incl/mutex.h"

// Buffers
volatile uint8_t inputBuffer[INPUT_BUFFER_SIZE+1];
volatile uint8_t outputBuffer[OUTPUT_BUFFER_SIZE+1];

uint8_t SERIAL_PID = 0;

volatile size_t inputBufferLength = 0;
volatile size_t outputBufferLength = 0;

size_t _outputBufferOffset = 0;

void USART_Transmit(uint8_t data);


void USART_Thread(void *parameter) {
	MUTEX_AWAIT(MUTEX_UART, 1, M_WRITE) {
		while(1) {
			if(_outputBufferOffset >= 10 || (_outputBufferOffset == outputBufferLength && outputBufferLength > 0)) {
				// shift buffer by 10 bytes if they were sent.
				memcpy((void *)outputBuffer, (void *) outputBuffer + _outputBufferOffset, outputBufferLength - _outputBufferOffset);
				outputBufferLength -= _outputBufferOffset;
				_outputBufferOffset = 0;
			}
			
			if(outputBufferLength == 0) {
				// if all data was sent, suspend task until new data comes in
				SUSPEND();
			} else {
				// transmit the next byte
				USART_Transmit(outputBuffer[_outputBufferOffset]);
				_outputBufferOffset++;
			}
		}
	}
}

void USART_Init(uint32_t baudrate) {
	uint32_t ubrr = F_CPU / 16 / baudrate - 1; // UBRRn calculation
	UBRR0H = (uint8_t) (ubrr >> 8); // Set baudrate high
	UBRR0L = (uint8_t) ubrr & 0xff; // Set baudrate low
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); // Enable receiver and transmitter + receive interrupt
	UCSR0C = (1 << USBS0)  | (1 << UCSZ01) | (1 << UCSZ00); // Set frame format: 8data, 2stop bit
}

void USART_Transmit(uint8_t data) {
	while (!(UCSR0A & (1<<UDRE0))); // Wait for empty transmit buffer (UDRE0 set if buffer empty / ready for new data)
	UDR0 = data; // Put data into buffer, sends the data
}

void USART_Send(uint8_t data) {
	outputBuffer[outputBufferLength] = data;
	outputBufferLength++;
	if(outputBufferLength >= OUTPUT_BUFFER_SIZE) {
		outputBufferLength = 0;
	}
	if(SERIAL_PID != 0) {
		RESUME(SERIAL_PID);
	}
}

uint8_t USART_Data_Available() {
	return inputBufferLength > 0;
}

char USART_Read() {
	char result = inputBuffer[0];
	
	inputBufferLength--;
	memcpy((void *)inputBuffer, (void *)inputBuffer + 1, inputBufferLength);
	
	return result;
}

ISR(USART_RX_vect) {
	if(inputBufferLength < INPUT_BUFFER_SIZE) {
		inputBuffer[inputBufferLength] = UDR0;
		inputBufferLength++;
		inputBuffer[inputBufferLength] = '\0';
	} else {
		// shift buffer if full
		memcpy((void *)inputBuffer, (void *)inputBuffer + 1, inputBufferLength - 1);
		inputBuffer[inputBufferLength - 1] = UDR0;
	}
}