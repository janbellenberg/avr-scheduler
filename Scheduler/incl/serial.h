/**
 * serial.h contains all methods for serial communication
 */

#ifndef SERIAL_H_
#define SERIAL_H_

#include <stdint.h>

#define MUTEX_UART 0x01

/**
 * @brief PID of the serial thread
 */
extern uint8_t SERIAL_PID;

/**
 * @brief Task for serial communication
 * @param parameter: not used
 * This tasks continously reads from the output buffer and sends the data.
 * If no data if available, the task is suspended, until USART_Send is called.
 */
void USART_Thread(void *parameter);

/**
 * @brief Initializes the USART
 * @param baudrate: baudrate of the USART
 * This method initializes the USART with the given baudrate.
 * The USART is configured for 8 data bits and 2 stop bits
 */
void USART_Init(uint32_t baudrate);

/**
 * @brief Sends a byte over the USART
 * @param data: byte to send
 * This methods enqueues the given byte in the output buffer and resumes the serial thread.
 */
void USART_Send(uint8_t data);

/**
 * @brief Checks, if data is available in the input buffer.
 * @return 1 if data is available, 0 otherwise.
 */
uint8_t USART_Data_Available();

/**
 * @brief Reads a byte from the input buffer
 * @return byte from the input buffer
 */
char USART_Read();

#endif /* SERIAL_H_ */