/*
 * taskmanager.h contains the methods to send updates
 * to the taskmanager application via usart.
 */ 


#ifndef TASKMANAGER_H_
#define TASKMANAGER_H_

#include <stdint.h>

#define ID_LOG			0x01		///< ID for log outputs
#define ID_FCPU			0x02		///< ID for updating the cpu frequency
#define ID_OS_TIME		0x03		///< ID for updating the os up time
#define ID_OS_USAGE		0x04		///< ID for updating the cpu usage
#define ID_START_TASK	0x05		///< ID for starting a new task
#define ID_STOP_TASK	0x06		///< ID for stopping a task
#define ID_SUSPEND_TASK	0x07		///< ID for suspending a task
#define ID_RESUME_TASK	0x08		///< ID for resuming a task
#define ID_TASK_TIME	0x09		///< ID for updating the task time
#define ID_TASK_NAME	0x0A		///< ID for updating the task name

/**
 * @brief Package structure for sending data to the taskmanager application.
*/
typedef struct {
	uint8_t ID;					///< Unique identifier for the data.
	uint8_t Ref;				///< Task identifier for the data.
	uint32_t Payload;		///< Data to be sent.
} Package;

/**
 * @brief Package structure for sending strings to the taskmanager application.
*/
typedef struct {
	uint8_t ID;			///< Unique identifier for the data.		
	uint8_t Ref;		///< Task identifier for the data.
	unsigned char ch0;	///< First character of the string.
	unsigned char ch1;	///< Second character of the string.
	unsigned char ch2;	///< Third character of the string.
	unsigned char ch3;	///< Fourth character of the string.
} StringPackage;

/**
 * @brief Raw structure for sending data to the taskmanager application.
*/
typedef struct {
	uint8_t byte0;
	uint8_t byte1;
	uint8_t byte2;
	uint8_t byte3;
	uint8_t byte4;
	uint8_t byte5;
} Raw;

/**
 * @brief Converter structure for sending data to the taskmanager application.
*/
typedef union {
	Package data;		///< Real data for sending data to the taskmanager application.
	Raw raw;				///< Converted raw data for sending data to the taskmanager application.
} Converter;

/**
 * @brief Send a package to the taskmanager application.
 * @param ID Unique identifier for the data.
 * @param Ref Task identifier for the data.
 * @param Payload Data to be sent.
*/
void sendPackage(uint8_t ID, uint8_t Ref, uint64_t Payload);

/**
 * @brief Send a string package to the taskmanager application.
 * @param ID Unique identifier for the data.
 * @param Ref Task identifier for the data.
 * @param Payload String to be sent.
*/
void sendString(uint8_t ID, uint8_t Ref, char Payload[4]);

/**
 * @brief Send a log message to the taskmanager application.
 * @param message Message to be sent.
*/
#define LOG(message) sendString(ID_LOG, 0, message);

#endif /* TASKMANAGER_H_ */