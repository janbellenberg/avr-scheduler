/**
 * scheduler_internal.h contains all kernel-related methods,
 * which SHOULDN'T be used in the user's code.
 */

#ifndef SCHEDULER_INTERNAL_H_
#define SCHEDULER_INTERNAL_H_

// don't use value 0 !!

/// States of tasks
typedef enum
{
  READY = 1,
  SUSPENDED = 2,
} Status;

/// Types of tasks
typedef enum
{
  IDLE = 1,
  TASK = 2,
  MICROTASK = 3
} Type;

/**
 * This struct contains all the
 * information about the tasks.
 */
typedef struct _task
{
  uint8_t *sp; ///< current stack pointer
  void *stack; ///< first address of stack array
  size_t stackSize;		///< size of the stack array
  uint8_t PID; ///< id of the task
  char name[TASK_NAME_LENGTH+1];	///< name of the task

  struct _task *parent;   ///< parent task
  struct _task *next;     ///< next task in linked list
  struct _task *previous; ///< previous task in linked list
  // the linked list is implemented cyclic,
  // so next of the last points to the first
  // and previous of the first points to the last.

  Type type;          ///< type of the task
  Status status;      ///< current state of the task
  uint64_t startedAt; ///< os tick, on which the task was created
  uint64_t usedSlots; ///< numbers of scheduling slots, the task has used
  volatile void *param;		///< pointer to the parameter of the task
  void **result;      ///< pointer to the result of subtasks

  Signal *lastSignal;          ///< entrypoint for the linked list of signals.
  void (*signalHandler)(void); ///< reference to the signal handler

  // Scheduling information
  uint64_t resumeAt;     ///< os tick, on which the task should be resumed after sleep
  Priority priority;     ///< priority for scheduling
  uint8_t waitingCycles; ///< number of scheduling cycles, the task if already waiting (priority scheduling)

  // Flags
  uint8_t RESUME_PARENT_ON_EXIT : 1; ///< flag, is the parent task awaiting the end of this task?
  uint8_t REQUEST_INTERRUPTION : 1;  ///< flag, was the interruption requested by another task?
} Task;

/**
 * Management information about the kernel.
 */
typedef struct
{
  uint8_t bootupComplete : 1;           ///< flag, is the kernel booted?
  volatile uint8_t waitForDispatch : 1; ///< flag, is any task waiting for a schedule event?

  volatile uint64_t os_time; ///< number of scheduling cycles, the os is running

  Task *firstTask;       ///< entrypoint for the linked list of tasks
  Task *lastTask;        ///< last item in the linked list of tasks
  Task *currentTask;     ///< pointer to the current running task
  uint8_t existingTasks; ///< number of existing tasks
  uint8_t pid_counter;   ///<	counter for the auto-increment of pid's
} KernelStatus;

extern volatile KernelStatus kernel; ///< kernel instance
extern volatile void *current_stack; ///< pointer to the current stack
extern uint64_t _os_time;            ///< os_time from last cpu usage check
extern uint64_t _idle_time;			 ///< idle time from last cpu usage check
extern uint16_t _timerDiff;			 ///< number of remaining ticks to regular scheduling, when YIELD was called to force early schedule
char MEMORY[MEMORY_SIZE];			 ///< memory for the dynamic memory management

/**
 * The idle task sends the mcu to sleep and wakes it up via Timer2
 * for the next scheduling cycle.
 * @brief IDLE-Task
 * @param param NULL
 */
void idle(void *param);

/**
 * @brief Resets the stack of the idle task, because it's always called from the beginning.
 */
void _resetIdleTaskStack(void);

/**
 * @brief Stops a task, if it returned.
 */
void _handleExit(void);

/**
 * @brief Used in shutdown process to switch to the afterShutdown function
 * @param functionPointer The function, which should be called after the shutdown.
 */
void _handleTimerShutdown(void *functionPointer);

/**
 * @brief Lookup of a task via the PID
 * @param PID ID of the task
 * @return Task-struct of the task.
 */
Task *_getTaskByID(uint8_t PID);

/**
 * @brief Configures a task (including its stack)
 * @param name identifier of the task.
 * @param function Function handle for the new task.
 * @param stackSize Size of the stack in bytes.
 * @param param Pointer to optional params.
 * @returns Configured task structure.
 */
Task *_configureTask(char name[TASK_NAME_LENGTH+1], void *function, uint16_t stackSize, volatile void *param);

/**
* @brief Configures the new task for fork
*/
void _configureFork(void);

/**
* @brief ASM Jump location for fork child task
*/
extern void _forkEnd(void);

/**
 * @brief Task for updating the taskmananger UI
 */
void _taskmgrTask(void *param);

//////////////////////////////////////////////////////////////////////////
/// 	                         Macros								   ///
//////////////////////////////////////////////////////////////////////////

#define CHECK_KERNEL_BOOT					if (!kernel.bootupComplete) { return; }
#define CHECK_KERNEL_BOOT_WERROR(code)		if (!kernel.bootupComplete) { return code; }
#define BEGIN_KERNEL_OPERATION				if (!kernel.bootupComplete) { return; } asm volatile("cli");
#define BEGIN_KERNEL_OPERATION_WERROR(code) if (!kernel.bootupComplete) { return code; } asm volatile("cli");
#define END_KERNEL_OPERATION				asm volatile("sei");

#endif /* SCHEDULER_INTERNAL_H_ */