/**
 * scheduler.h contains all kernel-related methods,
 * like os-management, task-management and
 * inter-task communication with signals.
 */

#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#ifndef __OPTIMIZE__
#warning "Compiler optimizations disabled; scheduler may not work due to memory limits"
#endif

#include "incl/settings.h"
#include "incl/taskmanager.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <avr/io.h>
#include <avr/interrupt.h>

typedef void (*FunctionHandle)(void);     ///< function handle without parameter or return value
typedef void (*TaskHandle)(void *);       ///< function handle with void* as parameter but without return value
typedef void *(*MicrotaskHandle)(void *); ///< funtion handle with void* as parameter and return value

/**
 * @brief Representation of a signal as an element of a linked list.
 */
typedef struct _signal
{
  uint8_t signal;
  void *payload;
  uint8_t senderPID;
  struct _signal *next;
} Signal;

/// Priorities of tasks for scheduling
typedef enum
{
	LOW = 1,
	DEFAULT = 2,
	HIGH = 3
} Priority;

//////////////////////////////////////////////////////////////////////////
///                              OS methods                            ///
//////////////////////////////////////////////////////////////////////////

/**
 * This method configures the os and the mcu to enable scheduling.
 * @brief starts the scheduling
 * @param setup function for setting up the mcu before the scheduling starts.
 */
void os_boot(FunctionHandle setup);

/**
 * This function forces the os to switch to another task, even if the current slot isn't elapsed.
 * @brief Switch task
 */
void YIELD();

/**
 * This function triggers the watchdog to reboot the whole mcu.
 * @brief Reboots the MCU
 */
void os_reboot(void);

/**
 * If the function is a nullptr, the function uses the power-down mode of the mcu to stop the processing,
 * otherwise the function just stops the scheduling and runs the afterShutdown function.
 * @brief Stops the scheduler
 * @param afterShutdown A function, which should be executed after the shutdown. Can be null.
 */
void os_shutdown(FunctionHandle afterShutdown);

/**
 * @brief Calculates the up time of the os.
 * @returns Duration in seconds
 */
double os_get_up_time(void);

/**
 * @brief Gets the scheduling ticks, that did already happen.
 * @returns ticks
 */
uint64_t os_get_real_ticks();

/**
 * Calculates the time, when the IDLE-task is not running.
 * @brief Calculates the workload of the processor
 * @returns Workload in %
 */
uint8_t cpuUsage(void);

//////////////////////////////////////////////////////////////////////////
///                           Task methods                             ///
//////////////////////////////////////////////////////////////////////////

/**
 * @brief Starts a new task.
 * @param name identifier of the task.
 * @param function Function handle for the new task.
 * @param stackSize Size of the stack in bytes.
 * @param param Pointer to optional params.
 * @returns ID of the new task.
 */
uint8_t startTask(char name[TASK_NAME_LENGTH+1], TaskHandle function, uint16_t stackSize, volatile void *param);

/**
 * @brief Starts a new microtask.
 * @param name identifier of the task.
 * @param function Function handle for the new task,
 * @param stackSiz Size of the stack in bytes.
 * @param param Pointer to optional params.
 * @param result Pointer to the place, where the result pointer should be stored.
 * @returns ID of the new task.
 */
uint8_t startMicrotask(char name[TASK_NAME_LENGTH+1], MicrotaskHandle function, uint16_t stackSize, volatile void *param, void **result);

/**
* @brief Duplicates the current task.
* @returns 0 for the current task, 1 for the new task
*/
extern uint8_t forkTask(void);
#define FORK_CHILD	0x01	///< forkTask returns 0x01 for the child process
#define FORK_PARENT 0x00	///< forkTask returns 0x00 for the parent process

/**
 * @brief Waits for the task to exit.
 * @param PID ID of the task.
 */
void await(uint8_t PID);

/**
 * @brief Suspends the task for a given duration.
 * @param milliseconds Duration in milliseconds.
 */
void sleep(uint16_t milliseconds);

/**
 * @brief Pauses the current task until RESUME is called.
 */
void SUSPEND();

/**
 * @brief Pauses the specified task until RESUME is called.
 * @param PID ID of the task, which should be paused.
 */
void PAUSE(uint8_t PID);

/**
 * @brief Resumes the specified task.
 * @param PID ID of the task.
 */
void RESUME(uint8_t PID);

/**
 * May cause memory leaks, please call requestInterruption before, if malloc was used.
 * @brief Kills the specified task.
 * @param PID ID of the task.
 */
void stopTask(uint8_t PID);

/**
 * @brief Updates the priority of the specified task.
 * @param PID ID of the task.
 * @param priority New priority.
*/
void setPriority(uint8_t PID, Priority priority);

//////////////////////////////////////////////////////////////////////////
///                          Signal methods                            ///
//////////////////////////////////////////////////////////////////////////

#define SIG_INTERRUPT 0x10 ///< signal identifier for requestInterruption

/**
 * @brief Sends a signal to a task.
 * @param PID ID of the receiver task.
 * @param signal 8-bit identifier of the signal.
 * @param value Optional payload for the signal.
 */
void sendSignal(uint8_t PID, uint8_t signal, void *value);

/**
 * @brief Fetches the next signal from the queue.
 * @returns The next signal.
 */
Signal *getNextSignal();

/**
 * @brief Register a handler-function, which should be called, if the task receives a signal.
 * @param function Reference to the handler function.
 */
void registerSignalHandler(FunctionHandle function);

/**
 * @brief Sends SIG_INTERRUPT to the task and sets the REQUEST_INTERRUPTION flag, to request the interruption.
 * @param PID ID of the receiver task.
 */
void requestInterruption(uint8_t PID);

/**
 * @brief Checks, if the REQUEST_INTERRUPTION was set.
 * @returns Whether the flag was set.
 */
uint8_t isInterruptionRequested();

#endif /* SCHEDULER_H_ */