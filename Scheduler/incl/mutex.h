/*
 * mutex.h provides all the methods for locking resources.
 * USE MACROS FOR MUTEX!
 */

#ifndef MUTEX

#include "incl/scheduler.h"
#include <stdlib.h>
#include <stdint.h>

/**
 * Requests the access to the resource,
 * if resource is blocked, the code will be skipped.
 * @param ID Unique resource ID
 * @param clients Number of instances, which should be acquired
 * @param mode Read/Write
 */
#define MUTEX(ID, clients, mode)    \
  uint8_t loops = 0;                \
  if (mutex_available(ID, clients)) \
    for (mutex_acquire(ID, clients, mode); loops < 1; loops++, mutex_release(ID, clients))

/**
 * Requests the access to the resource,
 * if resource is blocked, the task will wait, until the resource if free.
 * @param ID Unique resource ID
 * @param clients Number of instances, which should be acquired
 * @param mode Read/Write
 */
#define MUTEX_AWAIT(ID, clients, mode)        \
  uint8_t loops = 0;                          \
  while (mutex_available(ID, clients) < mode) YIELD(); \
  for (mutex_acquire(ID, clients, mode); loops < 1; loops++, mutex_release(ID, clients))

#define M_READ 0x01  ///< read mode
#define M_WRITE 0x02 ///< write mode

/**
 * @brief Register a device id for mutex
 * @param ID Unique id of the resource
 * @param maxConcurrentClients Number of clients, which should be able to read at the same time.
 */
void mutex_add_config(uint8_t ID, uint8_t maxConcurrentClients);

/**
 * @brief Check if the resource if free for the number of clients.
 * @param ID Unique id of the resource
 * @param clients Number of instances, which should be acquired
 * @returns the mode, which is available (write includes read, 0 is none)
 */
uint8_t mutex_available(uint8_t ID, uint8_t clients);

/**
 * @brief Acquires/locks the resource if available
 * @param ID Unique id of the resource
 * @param clients Number of instances, which should be acquired
 * @param mode Read/write
 * @returns true, if acquire was successful
 */
uint8_t mutex_acquire(uint8_t ID, uint8_t clients, uint8_t mode);

/**
 * @brief Releases the resource
 * @param ID Unique id of the resource
 * @param clients Number of instances, which should be released
 */
void mutex_release(uint8_t ID, uint8_t clients);

/**
 * Configuration of the resource.
 */
typedef struct _mutex
{
  uint8_t ID;                ///< Unique ID of the resource
  uint8_t maxClients;        ///< Max. number of clients for concurrent read
  uint8_t currentClients;    ///< Currently active clients
  uint8_t isInWriteMode : 1; ///< Whether a client is currently writing to the resource

  struct _mutex *next;     ///< Next item in the linked list
  struct _mutex *previous; ///< Previous item in the linked list
} MutexItem;

#endif /* MUTEX */