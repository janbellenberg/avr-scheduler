/**
 * settings.h contains several directives to configure the behavior of the os.
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#define IDLE_TASK_STACK 128            // stack size of the IDLE task
#define AFTER_SHUTDOWN_STACK_SIZE 256 // stack size of the afterShutdown task
#define MEMORY_SIZE 1600              // memory size, which can be allocated by malloc, etc
#define TASK_NAME_LENGTH 4			  // number of chars for the name of the task
#define WATCHDOG                      // the watchdog should be enabled

#ifdef __OPTIMIZE__
#define TASKMGR						  // the kernel should transmit data to the taskmanager via usart
#else
#warning "Taskmanager disabled"
#endif

#define INPUT_BUFFER_SIZE 10
#define OUTPUT_BUFFER_SIZE 200

// Scheduling algorithms
// #define SCHEDULER_FIFO     // first-in-first-out
// #define SCHEDULER_RR // round robin
#define SCHEDULER_RR_PRIO  // round robin with priorities
// #define SCHEDULER_SJF      // shortest job first

#endif /* SCHEDULER_SETTINGS_H_ */