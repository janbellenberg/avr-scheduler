#include "Task.h"

Task::Task(QString pid, QString name, TaskStatus status, quint32 cpuTime)
    : m_PID(pid)
    , m_name(name)
    , m_status(status)
    , m_cpuTime(cpuTime) {}

const QString &Task::getPID() const {
    return m_PID;
}

void Task::setPID(const QString &newPID) {
    m_PID = newPID;
}

const QString &Task::getName() const {
    return m_name;
}

void Task::setName(const QString &newName) {
    m_name = newName;
}

TaskStatus Task::getStatus() const {
    return m_status;
}

void Task::setStatus(TaskStatus newStatus) {
    m_status = newStatus;
}

quint32 Task::getCpuTime() const {
    return m_cpuTime;
}

void Task::setCpuTime(const quint32& newCpuTime) {
    m_cpuTime = newCpuTime;
}

QString Task::getStatusString() const {
    switch(this->m_status) {
    case TaskStatus::UNKNOWN:
        return "Unknown";
    case TaskStatus::RUNNING:
        return "Runnig";
    case TaskStatus::WAITING:
        return "Waiting";
    }
    return "Unknown";
}

bool operator==(const Task& task1, const Task& task2) {
    return task1.getPID() == task2.getPID();
}
