#include "PortTrackingThread.h"

PortTrackingThread::PortTrackingThread(QObject *parent) : QThread{parent} {}

void PortTrackingThread::run() {
    while(!QThread::currentThread()->isInterruptionRequested()) {
        const QList<QSerialPortInfo>& newPorts = QSerialPortInfo::availablePorts();

        if(m_portInfo.size() != newPorts.size()) {
            m_portInfo.clear();

            for(const QSerialPortInfo& element : newPorts) {
                m_portInfo.append(element.portName());
            }
            emit update(&m_portInfo);
        }

        for(const QSerialPortInfo& testelement : newPorts) {
            if(!m_portInfo.contains(testelement.portName())) {
                m_portInfo.clear();

                for(const QSerialPortInfo& element : newPorts) {
                    m_portInfo.append(element.portName());
                }
                emit update(&m_portInfo);
                break;
            }
        }
        QThread::msleep(250);
    }
}
