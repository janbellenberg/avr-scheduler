#include "TaskGraph.h"

TaskGraph::TaskGraph(QObject *parent) : QObject{parent}, m_view(new QChartView(new QChart)), m_series(new QLineSeries) {
    m_view->setMinimumHeight(250);
    m_view->setRenderHints(QPainter::Antialiasing);
    m_view->chart()->setTitle("CPU-Auslastung");
    m_view->chart()->legend()->hide();

    m_view->chart()->addSeries(m_series);
    repaint();

    m_timer.start(1000);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(repaint()));
}

TaskGraph::~TaskGraph() {
    delete m_series;
    delete m_view;
}

QChartView* TaskGraph::view() const {
    return m_view;
}

void TaskGraph::newCpuPercent(const qreal& percent) {
    if(percent > 100) return;
    m_percentage = percent;
}

void TaskGraph::reset() {
    m_percentage = 0.0;
}

void TaskGraph::repaint() {
    if(m_series->points().size() > intervall) {
        m_series->removePoints(0,1);
    }
    QList<QPointF> points = QList<QPointF>(m_series->points());
    m_series->clear();
    for(int i = 0; i < points.size(); i++) {
        m_series->append(points.at(i).x()-1, points.at(i).y());
    }

    m_series->append(intervall, m_percentage);

    // Remove and add series for repaint. Note: calling repaint() / update() doesnt work
    m_view->chart()->removeSeries(m_series);
    m_view->chart()->addSeries(m_series);

    // Axes need to be redone after repaint
    m_view->chart()->createDefaultAxes();
    m_view->chart()->axes(Qt::Vertical).first()->setRange(0, 100);
    m_view->chart()->axes(Qt::Horizontal).first()->setRange(0, intervall);
    m_view->chart()->axes(Qt::Horizontal).first()->hide();
}
