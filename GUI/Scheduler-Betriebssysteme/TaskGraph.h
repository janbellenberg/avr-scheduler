#ifndef TASKGRAPH_H
#define TASKGRAPH_H

#include <QObject>
#include <QtCharts>
#include <QDebug>

/**
 * @brief Class that visualizes cpu usage in percent.
 * @details Internally this class uses a QChartView to display a number of points defined in the intervall property.
 * A timer repaints this graph continously. The new percent values repainted onto the graph can be et via calling ewCpuPercent(const qreal& percent = 0).
 */
class TaskGraph : public QObject {
    Q_OBJECT

private:
    QChartView* m_view;
    QLineSeries* m_series;
    qreal m_percentage;

    QTimer m_timer;
    const quint16 intervall = 10;

public:
    explicit TaskGraph(QObject *parent = nullptr);

    virtual ~TaskGraph();

    QChartView* view() const;

public slots:
    /**
     * @brief Sets the intern m_percentage property which is used to generate the next points on the graph.
     * @param percent New percentage to paint
     */
    void newCpuPercent(const qreal& percent = 0);

    /**
     * @brief Basicly just sets the percentage to 0.
     */
    void reset();

private slots:
    /**
     * @brief Private slot called by the timer to repaint the graph with one new point.
     * @details The visualization is as follows: Each repaint moves all existing points to the left.
     * Each repaint adds one new piont on the right whose value will be the last set percentage.
     * Points that fall out the visualized space are removed.
     */
    void repaint();
};

#endif // TASKGRAPH_H
