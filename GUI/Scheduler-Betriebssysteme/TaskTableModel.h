#ifndef TASKTABLEMODEL_H
#define TASKTABLEMODEL_H

#include <QAbstractItemModel>
#include "Task.h"

/**
 * @brief Model for the displaying table of the application.
 * @details Roughly implemented to work for Qt. Displayed information is the Task PID, name, status and cpu time.
 * Information is taken from the SharedData class since this class stores a pointer to the task list.
 */
class TaskListModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit TaskListModel(QList<Task>* tasks, QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;

    QModelIndex parent(const QModelIndex& child) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    QList<Task>* m_tasks;
};

#endif // TASKTABLEMODEL_H
