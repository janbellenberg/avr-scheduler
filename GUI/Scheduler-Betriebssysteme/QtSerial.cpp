#include "QtSerial.h"

QDebug operator<<(QDebug dbg, const Payload& payload) {
    dbg << QString("Payload: " + QString::number(payload.id) + " | "
        + QString::number(payload.ref) + " | "
        + QString::number(payload.data) + " (" + QString::number(payload.data, 2).rightJustified(32, '0') + ")");
    return dbg;
}

QString SerialConnector::dataToString(const quint32& data) {
    QByteArray bytes;
    for(int i = 0; i < 4; i++) {
        quint8 byte = static_cast<quint8>((data >> (i*8)) & 0xFF);
        if(byte != 0) {
            bytes += byte;
        } else {
            break;
        }
    }
    return QString(bytes);
}

quint32 SerialConnector::stringToData(const QString& message) {
    quint32 data = 0;
    for(int i = 0; i < 4 && i < message.size(); i++) {
        data |= static_cast<quint32>(message.at(i).toLatin1()) << (8*i);
    }

    return data;
}

SerialConnector::SerialConnector(const quint32& baudrate) : m_port(new QSerialPort) {
    m_port->setBaudRate(baudrate);
    m_port->setReadBufferSize(SERIAL_BUFFER_SIZE);
    m_port->setDataBits(QSerialPort::DataBits::Data8);
    m_port->setParity(QSerialPort::Parity::NoParity);
    m_port->setStopBits(QSerialPort::StopBits::TwoStop);
    m_port->setFlowControl(QSerialPort::FlowControl::NoFlowControl);
    connect(m_port, SIGNAL(readyRead()), this, SLOT(read()));
    connect(m_port, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SLOT(catchError(QSerialPort::SerialPortError)));

#ifdef TESTING
    fakePayloads.enqueue(Payload{ID_LOG, 0, SerialConnector::stringToData("test")});
    fakePayloads.enqueue(Payload{ID_FCPU, 0, 8000000});
    fakePayloads.enqueue(Payload{ID_OS_TIME, 0, 123});
    fakePayloads.enqueue(Payload{ID_OS_USAGE, 0, 50});
    fakePayloads.enqueue(Payload{ID_START_TASK, 0, 1});
    fakePayloads.enqueue(Payload{ID_START_TASK, 0, 2});
    fakePayloads.enqueue(Payload{ID_TASK_NAME, 1, SerialConnector::stringToData("t1")});
    fakePayloads.enqueue(Payload{ID_TASK_NAME, 2, SerialConnector::stringToData("loooooong")});
    fakePayloads.enqueue(Payload{ID_TASK_TIME, 1, 85});
    fakePayloads.enqueue(Payload{ID_OS_USAGE, 0, 75});
    fakePayloads.enqueue(Payload{ID_OS_USAGE, 0, 100});
    fakePayloads.enqueue(Payload{ID_OS_USAGE, 0, 100});
    fakePayloads.enqueue(Payload{ID_TASK_TIME, 2, 21});
    fakePayloads.enqueue(Payload{ID_SUSPEND_TASK, 2, 0});
    fakePayloads.enqueue(Payload{ID_RESUME_TASK, 2, 0});
    fakePayloads.enqueue(Payload{ID_STOP_TASK, 2, 0});

    connect(&m_timer, SIGNAL(timeout()), this, SLOT(emitFakePayload()));
    m_timer.start(delay);
#endif
}

SerialConnector::~SerialConnector() {
    delete m_port;
}

bool SerialConnector::open(const QString& com) {
    if(m_port->isOpen()) return false;

    m_port->setPortName(com);
    m_port->open(QIODevice::ReadWrite);
    if(m_port->isOpen()) {
        m_buffer.clear();
        return true;
    } else {
        return false;
    }
}

void SerialConnector::close() {
    if(!this->m_port->isOpen()) return;
    this->m_port->close();
    emit disconnected();
}

void SerialConnector::sendData(const QString& data) {
    m_port->write(data.toUtf8());
}

void SerialConnector::read() {
    m_buffer += m_port->readAll();

    if(m_buffer.size() >= 6) {
        Payload payload{static_cast<quint8>(m_buffer[0]),static_cast<quint8>(m_buffer[1]), 0};

        for(int i = 2; i < 6; i++) {
            payload.data |= static_cast<quint32>(m_buffer[i]) << (i-2)*8;
        }

        emit payloadReceived(payload);

        m_buffer = m_buffer.mid(6);

        if(m_buffer.size() >= 6) {
            read();
        }
    }
}

bool SerialConnector::connected() const {
    return m_port->isOpen();
}

void SerialConnector::catchError(QSerialPort::SerialPortError error) {
    if(error == QSerialPort::SerialPortError::NoError) return;
    qDebug() << "Error: " << error << Qt::endl;
    this->close();
}

#ifdef TESTING
void SerialConnector::emitFakePayload() {
    if(!fakePayloads.empty()) {
        emit payloadReceived(fakePayloads.dequeue());
    } else {
        m_timer.stop();
    }
}
#endif
