#ifndef PORTTRACKINGTHREAD_H
#define PORTTRACKINGTHREAD_H

#include <QThread>
#include <QObject>
#include <QSerialPortInfo>

/**
 * @brief The PortTrackingThread class can be used to detect connected serial devices.
 * @details This thread checks the available serial ports in intervalls. If a new port is detected
 * or a port is disconnected, then the update() signal is called with a pointer to the list containing
 * all port names that are currently available.
 */
class PortTrackingThread : public QThread {
    Q_OBJECT
private:
    QList<QString> m_portInfo;

public:
    explicit PortTrackingThread(QObject *parent = nullptr);

    void run() override;

signals:
    /**
     * @brief Holds list pointer to all active ports sicnce last check
     */
    void update(QList<QString>*);
};

#endif // PORTTRACKINGTHREAD_H
