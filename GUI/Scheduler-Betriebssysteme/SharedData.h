#ifndef SHAREDDATA_H
#define SHAREDDATA_H

#include <QObject>
#include <QList>
#include "Task.h"

/**
 * @brief Singelton that exists in order to store the task information.
 */
class SharedData : public QObject {
    Q_OBJECT

private:
    QList<Task> m_list;

    SharedData();

public:
    /**
     * @return Pointer to intern list of tasks objects.
     */
    QList<Task>* tasks();

    /**
     * @brief Searches for a specific PID and returns a pointer to this task if present.
     * @param PID Id of task to search for
     * @return Pointer to task if found, nullptr otherwise.
     */
    Task* getTaskByPID(const QString& PID);

    /**
     * @brief Getter function for receiving the instance of this data holder
     * @return Always returns the same instance of SharedData
     */
    static SharedData* instance();

public slots:
     /**
     * @brief Appends the given task to the intern list and causes taskAdded(Task*) to be emitted.
     * @param task Reference of the task to add
     */
    void addTask(const Task& task);

     /**
     * @brief Removes the given task from the intern list and causes taskRemoved(QString) to be emitted.
     * @param task Reference of the task to remove
     */
    void removeTask(const Task& task);

signals:
    /**
     * @brief Holds a pointer to the new task.
     */
    void taskAdded(Task*);

    /*
     * @brief Holds PID of deleted task.
     */
    void taskRemoved(QString);
};

#endif // SHAREDDATA_H
