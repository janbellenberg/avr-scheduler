#include "TaskTableModel.h"

TaskListModel::TaskListModel(QList<Task>* tasks, QObject *parent)
    : QAbstractItemModel(parent)
    , m_tasks(tasks) {}

QVariant TaskListModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if(role == Qt::DisplayRole) {
        if(orientation == Qt::Horizontal) {
            switch(section) {
            case 0: return "PID";
            case 1: return "Name";
            case 2: return "Status";
            case 3: return "CPU Zeit";
            }
        } else {
            return QString::number(section + 1);
        }
    }
    return QVariant();
}

QModelIndex TaskListModel::index(int row, int column, const QModelIndex& parent) const {
    return createIndex(row, column);
}

QModelIndex TaskListModel::parent(const QModelIndex& child) const {
    return QModelIndex();
}

int TaskListModel::rowCount(const QModelIndex &parent) const {
    return m_tasks->size();
}

int TaskListModel::columnCount(const QModelIndex &parent) const {
    return 4;
}

QVariant TaskListModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole) {
        const Task& task = m_tasks->at(index.row());
        switch(index.column()) {
        case 0: return task.getPID();
        case 1: return task.getName();
        case 2: return task.getStatusString();
        case 3: return task.getCpuTime();
        }
    }

    return QVariant();
}
