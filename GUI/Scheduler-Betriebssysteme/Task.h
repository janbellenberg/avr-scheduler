#ifndef TASK_H
#define TASK_H

#include <QString>

enum TaskStatus {
    RUNNING,
    WAITING,
    UNKNOWN,
};

/**
 * @brief The Task model class, wich just holds information about a task.
 * @details The task information consists of its PID, name, status and occupied cpu time.
 */
class Task {
private:
    QString m_PID;
    QString m_name;
    TaskStatus m_status;
    quint32 m_cpuTime;

public:    
    Task(QString pid = "0", QString name = "none", TaskStatus status = TaskStatus::RUNNING, quint32 cpuTime = 0);

    const QString &getPID() const;
    void setPID(const QString &newPID);
    const QString &getName() const;
    void setName(const QString &newName);
    TaskStatus getStatus() const;
    void setStatus(TaskStatus newStatus);
    quint32 getCpuTime() const;
    void setCpuTime(const quint32& newCpuTime);

    QString getStatusString() const;
};

bool operator==(const Task& task1, const Task& task2);

#endif // TASK_H
