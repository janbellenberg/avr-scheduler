#include "SharedData.h"

SharedData::SharedData() {}

QList<Task>* SharedData::tasks() { return &this->m_list; }

Task* SharedData::getTaskByPID(const QString& PID) {
    for(Task& task : m_list) {
        if(PID == task.getPID()) {
            return &task;
        }
    }
    return nullptr;
}

void SharedData::addTask(const Task& task) {
    m_list.append(task);
    emit taskAdded(const_cast<Task*>(&task));
}

void SharedData::removeTask(const Task& task) {
    QString PID = task.getPID();
    if(m_list.removeOne(task)) {
        emit taskRemoved(PID);
    }
}

SharedData* SharedData::instance() {
    static SharedData* instance = new SharedData();
    return instance;
}
