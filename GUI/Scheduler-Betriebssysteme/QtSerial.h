#ifndef QTSERIAL_H
#define QTSERIAL_H

// Qt sided implementation of serial port communication in a specialized format. Connection tested with ATmega328P.

//#define TESTING // Testpayloads

#include <QSerialPort>
#include <QObject>
#include <QDebug>

#ifdef TESTING
#include <QTimer>
#include <QQueue>
#endif

#define ID_LOG 0x01
#define ID_FCPU 0x02
#define ID_OS_TIME 0x03
#define ID_OS_USAGE 0x04
#define ID_START_TASK 0x05
#define ID_STOP_TASK 0x06
#define ID_SUSPEND_TASK 0x07
#define ID_RESUME_TASK 0x08
#define ID_TASK_TIME 0x09
#define ID_TASK_NAME 0x0A

#define SERIAL_BUFFER_SIZE 1024

/**
 * @brief Structure that holds info transmitted via the serial port.
 */
struct Payload {
    quint8 id;
    quint8 ref;
    quint32 data;
};

QDebug operator<<(QDebug dbg, const Payload& payload); //< Make Payload struct printable

/**
 * @brief The SerialConnector class that is used for connecting to serial devices.
 * @details Requires a name to connect to after initialization. Reads data in a special format.
 */
class SerialConnector : public QObject {
    Q_OBJECT

private:
#ifdef TESTING
    QTimer m_timer;
    QQueue<Payload> fakePayloads;
    quint32 delay = 1000;
#endif
    QSerialPort* m_port;
    QByteArray m_buffer;

public:
    /**
     * @brief dataToString Converts a 32 bit integer into a string converting every 8 bit into a character.
     * @param data Number to convert into a string
     * @return Data as string
     */
    static QString dataToString(const quint32& data);

    /**
     * @brief stringToData Converts a string to a 32 bit integer representation. Result only holds info for up to 4 characters.
     * @param message String to convert
     * @return Resulting 32 bit number
     */
    static quint32 stringToData(const QString& message);

    /**
     * @brief Constructor for initializing the connector for serial communication.
     * Currently implemented format: 8 Databits, no Paritybit, two Stopbits and no flowcontrol.
     * @param baudrate Baudrate for communication
     */
    SerialConnector(const quint32& baudrate);

    virtual ~SerialConnector();

    /**
     * @brief Tries to open a serial port with the provided name.
     * @param com Name of the port to connect to
     * @return Returns true if port was successfully opened
     */
    bool open(const QString& com);

    /**
     * @brief Closes any active connection.
     */
    void close();

    /**
     * @return True if a serial port is currently open
     */
    bool connected() const;

signals:
    /**
     * @brief As soon as 6 bytes are received this signal will be called with a payload containing all bytes received up to that point.
     */
    void payloadReceived(Payload);

    /**
     * @brief Emitted as soon as an error occurs. For example the serial device loosing connection.
     */
    void disconnected();

public slots:
    /**
     * @brief Sends data to the currently opened serial port.
     * @param data Data to be send
     */
    void sendData(const QString& data);

private slots:
    /**
     * @brief Is called as soon as data can be read from the serial port. Data is kept in the buffer till 6 bytes [size of Payload struct]
     * are read. Then payloadReceived() is emitted.
     */
    void read();

    /**
     * @brief If any error occurs this is called and causes the connector to close the connection and emit disconnected().
     */
    void catchError(QSerialPort::SerialPortError error);
    
#ifdef TESTING
    void emitFakePayload();
#endif
};

#endif // QTSERIAL_H
