#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "QtSerial.h"
#include "PortTrackingThread.h"
#include "TaskTableModel.h"
#include "SharedData.h"
#include "TaskGraph.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

/**
 * @brief Main frame of the appplication.
 * @details Serial communiction is managed through the SerialConnector class,
 * Data is stored in the SharedData singelton and the TaskGraph class displays the cpu usage in a fancy way.
 * @see SerialConnector
 * @see SharedData
 * @see TaskGraph
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    /**
     * @brief Resets most ui elements to a default value.
     * @details Clears the task list of the SharedData singelton as such also removes all displayed tasks.
     * Other elements will be set to display 0 / 0% / 0 Hz etc.
     * Note: Does not close any active connections.
     */
    void resetUI();

private:
    Ui::MainWindow *ui;
    SerialConnector* connector;
    PortTrackingThread* portThread;
    TaskGraph* graph;

    quint16 waitingTaskCount = 0;
    quint16 runningTaskCount = 0;
    quint64 cpuTime = 0;

public slots:
    /**
     * @brief Automaticly displayes all available ports
     */
    void updatePorts(QList<QString>* newPorts);

    /**
     * @brief Tries to open a specific port with the SerialConnector.
     */
    void openSerialport();

    /**
     * @brief Closes the opened port of the SerialConnector.
     * @details Also calls the restUi() method.
     */
    void closeSerialport();

    /**
     * @brief Main method for correctly interpreting and displaying the data received from the serial port.
     */
    void processPayload(Payload payload);

private slots:
    /**
     * @brief Prints a received short message to the logging display.
     */
    void log(const QString& message);

    /**
     * @brief Sends a "S" character to the serial port posing as the shutdown command.
     * @details Also calls the restUi() method.
     */
    void sendShutdown();

    /**
     * @brief Sends a "R" character to the serial port posing as the restart command.
     * @details Also calls the restUi() method.
     */
    void sendRestart();

    /**
     * @brief Sends a "C" character to the serial port posing as the start calculation command.
     */
    void sendStartCalculation();

    /**
     * @brief Needs to be called to update changes done to the database to table.
     */
    void repaintTaskTable();
};
#endif // MAINWINDOW_H
