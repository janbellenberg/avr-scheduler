#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , connector(new SerialConnector(QSerialPort::BaudRate::Baud9600))
    , portThread(new PortTrackingThread)
    , graph(new TaskGraph)
{
    ui->setupUi(this);
    ui->tasks_table->setModel(new TaskListModel(SharedData::instance()->tasks(), this));    
    ui->verticalLayout->insertWidget(ui->verticalLayout->count()-2, graph->view());

    connect(ui->connect_button, SIGNAL(pressed()), this, SLOT(openSerialport()));
    connect(ui->disconnect_button, SIGNAL(pressed()), this, SLOT(closeSerialport()));
    connect(ui->clear_button, SIGNAL(pressed()), ui->logs_textedit, SLOT(clear()));
    connect(connector, SIGNAL(disconnected()), this, SLOT(closeSerialport()));

    connect(ui->calculate_button, SIGNAL(pressed()), this, SLOT(sendStartCalculation()));
    connect(ui->shutdown_button, SIGNAL(pressed()), this, SLOT(sendShutdown()));
    connect(ui->restart_button, SIGNAL(pressed()), this, SLOT(sendRestart()));

    connect(portThread, SIGNAL(update(QList<QString>*)), this, SLOT(updatePorts(QList<QString>*)));
    connect(connector, SIGNAL(payloadReceived(Payload)), this, SLOT(processPayload(Payload)));

    portThread->start(QThread::LowPriority);
}

MainWindow::~MainWindow() {
    delete ui;
    delete connector;
    portThread->requestInterruption();
    portThread->quit();
    portThread->wait();
    delete portThread;
}

void MainWindow::updatePorts(QList<QString>* newPorts) {
    const QString old = ui->serialport_combobox->currentText();
    ui->serialport_combobox->clear();
    for(const QString& name : *newPorts) {
        ui->serialport_combobox->addItem(name);
    }
    ui->serialport_combobox->setCurrentText(newPorts->contains(old) ? old : "");
    if(!newPorts->contains(old) && this->connector->connected()) {
        this->connector->close();
    }
}

void MainWindow::openSerialport() {
    if(connector->open(ui->serialport_combobox->currentText())) {
        ui->serialport_combobox->setEnabled(false);
        ui->connect_button->setEnabled(false);
        ui->disconnect_button->setEnabled(true);
        ui->connection_status_display_label->setText("Verbunden");
        QPalette palette;
        palette.setColor(QPalette::WindowText, Qt::darkGreen);
        ui->connection_status_display_label->setPalette(palette);
    }
}

void MainWindow::closeSerialport() {
    connector->close();
    ui->serialport_combobox->setEnabled(true);
    ui->connect_button->setEnabled(true);
    ui->disconnect_button->setEnabled(false);
    ui->connection_status_display_label->setText("Getrennt");
    QPalette palette;
    palette.setColor(QPalette::WindowText, Qt::red);
    ui->connection_status_display_label->setPalette(palette);

    resetUI();
}

void MainWindow::resetUI() {
    cpuTime = 0;
    runningTaskCount = 0;
    waitingTaskCount = 0;

    ui->task_display_label->setText("0");
    ui->runningTask_display_label->setText("0");
    ui->taskWaiting_display_label->setText("0");
    ui->cpuUtilization_display_label->setText("0%");
    ui->runtime_display_label->setText("0");
    ui->speed_display_label->setText("0 Hz");

    SharedData::instance()->tasks()->clear();
    graph->reset();
    repaintTaskTable();
}

void MainWindow::log(const QString& message) {
    ui->logs_textedit->append(message);
}

void MainWindow::sendShutdown() {
    this->connector->sendData("S");
    log("Sending shutdown command...");

    resetUI();
}

void MainWindow::sendRestart() {
    this->connector->sendData("R");
    log("Sending restart command...");

    resetUI();
}

void MainWindow::sendStartCalculation() {
    this->connector->sendData("C");
    log("Sending start calculation command...");
}

void MainWindow::processPayload(Payload payload) {
    qDebug() << payload;

    switch(payload.id){
    case ID_LOG:
        log(SerialConnector::dataToString(payload.data));
        break;
    case ID_FCPU:
        ui->speed_display_label->setText(QString::number(payload.data) + " Hz");
        break;
    case ID_OS_TIME:
        cpuTime = payload.data;
        ui->runtime_display_label->setText(QString::number(cpuTime));
        break;
    case ID_OS_USAGE:
        ui->cpuUtilization_display_label->setText(QString::number(payload.data) + "%");
        graph->newCpuPercent(static_cast<qreal>(payload.data));
        break;
    case ID_START_TASK:
        SharedData::instance()->addTask(Task(QString::number(payload.data)));
        ui->task_display_label->setText(QString::number(SharedData::instance()->tasks()->size()));
        ui->runningTask_display_label->setText(QString::number(++runningTaskCount));
        repaintTaskTable();
        break;
    case ID_SUSPEND_TASK:
    {
        Task* task = SharedData::instance()->getTaskByPID(QString::number(payload.ref));
        if(task == nullptr || task->getStatus() == TaskStatus::WAITING) return;
        task->setStatus(TaskStatus::WAITING);
        ui->runningTask_display_label->setText(QString::number(--runningTaskCount));
        ui->taskWaiting_display_label->setText(QString::number(++waitingTaskCount));
        repaintTaskTable();
    }
        break;
    case ID_RESUME_TASK:
    {
        Task* task = SharedData::instance()->getTaskByPID(QString::number(payload.ref));
        if(task == nullptr || task->getStatus() == TaskStatus::RUNNING) return;
        task->setStatus(TaskStatus::RUNNING);
        ui->runningTask_display_label->setText(QString::number(++runningTaskCount));
        ui->taskWaiting_display_label->setText(QString::number(--waitingTaskCount));
        repaintTaskTable();
    }
        break;
    case ID_STOP_TASK:
    {
        Task* task = SharedData::instance()->getTaskByPID(QString::number(payload.ref));
        if(task == nullptr) return;
        SharedData::instance()->removeTask(*task);
        ui->runningTask_display_label->setText(QString::number(--runningTaskCount));
        repaintTaskTable();
    }
        break;
    case ID_TASK_NAME:
    {
        Task* task = SharedData::instance()->getTaskByPID(QString::number(payload.ref));
        if(task == nullptr) return;
        task->setName(SerialConnector::dataToString(payload.data));
        repaintTaskTable();
    }
        break;
    case ID_TASK_TIME:
    {
        Task* task = SharedData::instance()->getTaskByPID(QString::number(payload.ref));
        if(task == nullptr) return;
        task->setCpuTime(payload.data);
        repaintTaskTable();
    }
        break;
    }
}

void MainWindow::repaintTaskTable() {
    ui->tasks_table->model()->layoutChanged();
}
